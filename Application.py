from DatabaseAccess import DatabaseAccess
import sys

db = DatabaseAccess()

if __name__ == "__main__":
    id = sys.argv[1]

result = db.getAccesses(id)
if result != 1:
    print("The ID "+str(id)+" has accessed the database "
    +str(result)+" times")
else:
    print("This is the first time the ID "+str(id)+ " has accessed the database.\
    Welcome, "+str(id)+"!")
