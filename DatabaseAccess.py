from pymongo import MongoClient


# This is only used upon itialization, in order to have some data present

# savedIds.delete_many({})
#
# for i in range(0, 5):
#     print(i)
#     record = {"id": i, "accesses": 1}
#     savedIds.insert_one(record)

class DatabaseAccess:

    # Connects to the database using admin privileges

    def __init__(self):
        self.client = MongoClient("mongodb+srv://admin:ProcessoESviluppoSoftware20192020@1stassignmentpssw-c4r4n.gcp.mongodb.net/test?retryWrites=true&w=majority")
        self.db = self.client.accesses
        self.savedIds = self.db.accesses



    """
    Given an id, returns the number of times the ID has accessed the database
    Throws exception if the ID is not an integer
    """
    def getAccesses(self, id):

        # Selects which collection to use


        # throw error if the provided id is not an integer
        try:
            id = int(id)
        except Exception as e:
            raise Exception("Id should be an integer!")


        # Queries the DB
        result = self.savedIds.find_one({"id": id})

        if result != None:
            # get the number of accesses for specified ID
            accesses = result["accesses"]

            # increments the number of accesses
            self.savedIds.replace_one({"id": id, "accesses": accesses},
            {"id": id, "accesses": accesses + 1})

            # returns the number of accesses
            return accesses+1
            # print("The ID "+str(id)+" has accessed the database "
            # +str(accesses+1)+" times")

        else:
            # if this is the first time for provided ID, return 1
            self.savedIds.insert_one({"id": id, "accesses": 1})
            return 1
            # print("This is the first time the ID "+str(id)+ " has accessed the database.\
            # Welcome, "+str(id)+"!")


    def deleteAllIds(self):
        print(self.savedIds.count_documents)
        self.savedIds.delete_many({})

    def getAllIds(self):
        return self.savedIds.distinct("id")
